//Declare dependencies
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const multer = require("multer")
//----------------------------------------


//Connect to DB
mongoose.connect("mongodb://localhost:27017/mern2_tracker", 
{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true}); //DeprecationWarning

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});
//-----------------------------------------

//Apply Middleware (auth, validation) Request response pipeline
app.use(express.json());
app.use(express.urlencoded({extended: true}));
//-----------------------------------------

//Declare Models
const Team = require("./models/teams");
const Task = require("./models/tasks");
const Member = require("./models/members");
//-----------------------------------------

//Create Routes/Endpoints
//Transferred Routes
//Declare the resources
const teamsRoute = require("./routes/teams")
app.use("/teams", teamsRoute)
const tasksRoute = require("./routes/tasks")
app.use("/tasks", tasksRoute)
const membersRoute = require("./routes/members")
app.use("/members", membersRoute)

//Configure Multer (we can configure where the file can be saved)
const upload = multer({
	dest: "images/members", // "dest:" - destination
	limits: {
		filesize: 1000000 //max file size in bytes
	},
	fileFilter(req, file, cb) {
		//https://regex101.com
		if(!file.originalname.match(/\.(jpg|jpeg|png|PNG|JPEG)$/)) {
			return cb(new Error("Please upload an image only!"))
		}

		cb(undefined, true)

	}
})

//Sample: Endpoint to upload a file
app.post("/upload", upload.single("secretfile"), (req,res) => {
	// try {
	// 	res.send({ message: "Successfully uploaded image!" })
	// } catch(e) {
	// 	//BAD REQUEST
	// 	res.status(400).send({ error: e.message })
	// }
	res.send({ message: "Successfully uploaded image!" })
}, (error, req, res, next) => {
	res.status(400).send({error: message})
})  // upload a file

//Initialize the server
app.listen(4024, () => {
	console.log("Now listening to port 4024 :)");
});

